USE [DBForExample]
GO
/****** Object:  UserDefinedFunction [dbo].[GetStringStatus]    Script Date: 3/28/2021 12:06:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetStringStatus] (@input int)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Status VARCHAR(250)
	Set @Status = 'Undefine'
	IF @input = 0 --Or @Temp = 2 also?
BEGIN
    SET @Status = 'None'
END
	IF @input = 1 --Or @Temp = 2 also?
BEGIN
    SET @Status = 'Active'
END
	IF @input = 2 --Or @Temp = 2 also?
BEGIN
    SET @Status = 'Disable'
END
    RETURN @Status
END