﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DemoParralelTaskConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            await ShowThreadInfoWithAsync1("Some");
            Console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(600);
            //Example2();
            //Example1();
        }

        static async Task<string> ShowThreadInfoWithAsync1(String s)
        {
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            string id = await ShowThreadInfoWithAsync2(s);
            //int k = 0;
            //int b = 5 / k;
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            //id += await Task.Run(() => Thread.CurrentThread.ManagedThreadId.ToString());
            id += ShowThreadInfoWithAsync2(s);
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            return id;
        }

        static async Task<string> ShowThreadInfoWithAsync2(String s)
        {
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            string id = await Task.Run(() =>
            {
                //int k = 0;
                //int b = 5 / k;
                Thread.Sleep(1000);
                return Thread.CurrentThread.ManagedThreadId.ToString();
            });
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            return id;
        }

        static void Example2()
        {
            Console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            var t = Task.Run(() => ShowThreadInfoWithResult("Task"));
            t.Wait();
            Console.WriteLine("Hello World!" + t.Result);
        }

        static string ShowThreadInfoWithResult(String s)
        {
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            return Thread.CurrentThread.ManagedThreadId.ToString();
        }

        static void Example1()
        {
            Console.WriteLine($"Thread ID: {Thread.CurrentThread.ManagedThreadId}");
            var t = Task.Run(() => ShowThreadInfo("Task"));
            t.Wait();
            Console.WriteLine("Hello World!");
        }

        static void ShowThreadInfo(String s)
        {
            Console.WriteLine($"{s} Thread ID: {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}
